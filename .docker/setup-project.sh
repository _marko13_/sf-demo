#!/bin/bash
set -e

MIGRATION_RESULT=0
php /var/www/html/bin/console doctrine:migrations:migrate --no-interaction || MIGRATION_RESULT=$?

if [ $MIGRATION_RESULT -ne 0 ]; then
  echo "Migration failed. Stopping the container."
  exit 1
fi