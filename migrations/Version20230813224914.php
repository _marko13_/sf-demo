<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230813224914 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add new column comments and copy all content from notes to comments';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE symfony_demo_user ADD comments VARCHAR(255) COLLATE utf8mb4_unicode_ci NOT NULL AFTER notes');
        sleep(30);
        $this->addSql('UPDATE symfony_demo_user SET comments = notes');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE symfony_demo_user DROP COLUMN comments');
    }
}
