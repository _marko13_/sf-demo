<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230813225335 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Remove obsolete column notes';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE symfony_demo_user DROP COLUMN notes');
        sleep(50);
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE symfony_demo_user ADD notes VARCHAR(255) COLLATE utf8mb4_unicode_ci NOT NULL AFTER password');
        $this->addSql('UPDATE symfony_demo_user SET notes = comments');
    }
}
