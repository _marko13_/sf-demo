FROM registry.gitlab.com/zeacom/docker_phpfpm:8.2

COPY ./.docker/nginx_vhosts_default.conf /etc/nginx/vhosts.d/default.conf
COPY ./.docker/setup-project.sh /docker/scripts/setup-project2.sh
RUN chmod +x /docker/scripts/setup-project2.sh
RUN touch .env
COPY . /var/www/html
